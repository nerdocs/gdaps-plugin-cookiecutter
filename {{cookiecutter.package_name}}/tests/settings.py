"""
Testing settings for {{ cookiecutter.project_name }}.
"""

# inherit tests settings from {{ cookiecutter.project_title }}
from {{ cookiecutter.project_slug }}.tests.settings import *  # noqa


# use a name suitable for {{ project_title }} - {{ cookiecutter.plugin_title }}.
DATABASES["default"]["NAME"] = "medux-{{ cookiecutter.app_name }}.db"  # noqa
