# {{ cookiecutter.plugin_title }} Plugin


{{ cookiecutter.description }}

## General

{{ cookiecutter.project_title }} modules are living in the `{{ cookiecutter.plugin_namespace }}` setuptools entrypoint group.
They are normal Django apps, but found and loaded dynamically during startup when installed via pip.
As Django apps, they can have everything a "static" app also has:

#### {{ cookiecutter.__camel_cased_app_name }} PluginConfig

The app configuration is declared in the module's `apps.py`. Set the plugin's metadata there in the PluginMeta class.


## Install

You can install this plugin locally during development into MedUX by invoking `pip install -e .` in this folder, assuming you have activated your main project's virtualenv.

If you want to test this plugin on its own, you can also use a separate virtualenv for it:

```bash
virtualenv .venv
. .venv/bin/activate
pip install -e .
```

This should install {{ cookiecutter.project_title }} {{ cookiecutter.plugin_title}} editable in the virtual environment.

## Testing

Testing is done using pytest. Just call it in the root dir:
```
pytest
```

## Models

Create your models as usual in `models.py`, they will be included. Don't forget to run `makemigrations` and `migrate` afterwords.


After each change regarding version number etc., run `python manage.py syncplugins`. This ensures your database is in sync with the plugins on disk.

## License

This plugin is licensed under the {% if cookiecutter.license == "AGPLv3+" %}[GNU Affero General Public License v3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.txt){% elif cookiecutter.license == "GPLv3+" %}[GNU General Public License v3 or later (GPLv3+)](https://www.gnu.org/licenses/gpl-3.0.txt){% endif %}

