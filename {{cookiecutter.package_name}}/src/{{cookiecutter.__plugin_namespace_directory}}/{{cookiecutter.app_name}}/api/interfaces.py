from gdaps.api import Interface

# You can place your interfaces here, to be included from dependent plugins

# from gdaps import Interface
# @Interface
# class IMySpecial{{ cookiecutter.__camel_cased_app_name }}Interface:
#     """Please provide a detailed documentation of the interface."""
#
#     def do_something(self):
#         """Documentation of method"""
#
#
#     Then implement this interface in another, dependent plugin:
#
#     from .api.interfaces import IMySpecial{{ cookiecutter.__camel_cased_app_name }}Interface
#
#     class OtherPluginClass(IMySpecial{{ cookiecutter.__camel_cased_app_name }}Interface):
#
#        def do_something(self):
#            """do something."""
#
