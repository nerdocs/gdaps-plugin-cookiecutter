"""{{ cookiecutter.description }}"""
# For plugin apps, the AppConfig is found automatically.
# default_app_config = (
#     "{{ cookiecutter.plugin_namespace }}.{{ cookiecutter.app_name }}.apps.{{ cookiecutter.__camel_cased_app_name }}Config"
# )

__version__ = "{{ cookiecutter.plugin_version }}"
__license__ = "{{ cookiecutter.license }}"
