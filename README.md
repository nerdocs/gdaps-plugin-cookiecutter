# gdaps-plugin-cookiecutter


A [cookiecutter](https://github.com/pretix/pretix-plugin-cookiecutter) template for creating plugins for [GDAPS](https://gdaps.readthedocs.io) enabled Django applications.

This package eases the work that has to be done when creating a plugin for your GDAPS/Django project and sets up sensible defaults after asking a few questions.

### Usage
    
    cookiecutter gl:nerdocs/gdaps-plugin-cookiecutter

You are then asked a few short questions.

### Variables

##### project_title (*Default*: Your GDAPS Project)
Use a Human readable name here. This name is used to calculate other variables, like the project slug.

##### plugin_title (*Default*: Sample plugin)
This is the human readable name for your plugin app

##### project_slug (*Default*: machine redable version of `project_title`)
This should match the module name of your main application

##### plugin_namespace (*Default*: `project_slug`.plugins)
This namespace can be freely chosen. In most applications the default is ok. If you don't want to have a `my_app.plugins.foo_plugin` subfolder, but the `my_app.foo_plugin` folder directly under the main namespace, enter the main namespace here: `my_app`

##### app_name
*Default*: lowercased/machine named `plugin_title`

The plugin app name you want to use. This is the name Django sees it. 

##### package_name (*Default*: `project_slug-app_name` with hyphens instead of underlines)
The PyPi package name.

##### description
A short description of your plugin. This is used as description in PyPi and your README file.

##### author
Your name. You should add that to your .cookiecutterrc file as default. See [Defaults](#Defaults).

##### author_email
Your email address. You should add that to your .cookiecutterrc file as default. See [Defaults](#Defaults).

##### vendor
Your company. You should add that to your .cookiecutterrc file as default. See [Defaults](#Defaults).

##### category (*Default*: Base)
Category of your plugin. Choose freely.

##### license
Choose between a few licenses. PRs for others welcome.

##### plugin_version (*Default*: 0.0.1)
I recommend [Semantic versioning](https://semver.org/)


### Install

If you want to install your plugin for development, just do a

```bash
pip install -e .
```

in the plugin directory, with your main application's virtualenv enabled. Done.

For uploading to PyPi, follow [their guide](https://upload.pypi.org/help/#publishing).

### Defaults

Filling in  project title, author etc. again and again can be a tedious task. You can provide defaults for cookiecutter by creating a `.cookiecutterrc` file into your home directory with that context:

```
default_context:
  project_title: 
  author: Your Full Name
  email: your_email@example.com
  vendor: Your Company
  license: AGPLv3+

```

### Contribute

Feel free to form this repo and make your own enhancements.
If you want to just use it for a Django project with GDAPS, better use the Defaults hint above, this should do it. But any improvements, bug reports, other needed licenses in the list etc. are welcome, just drop a shor tissue and a PR!

### License

This template is released under the GPLv3 (see [LICENSE](LICENSE) file). You can freely use this tempate with cookiecutter to create software - That created plugin is not bound to any license. However, if you create a fork of this template and release it, you have to make your changes public, according to the GPL.

### Credits

Thanks to these really cool projects:

* [Cookiecutter](https://cookiecutter.readthedocs.io)


