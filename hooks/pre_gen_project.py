#!/usr/bin/env python3
import re
import sys


identifiers = {}
identifiers["project_slug"] = "{{ cookiecutter.project_slug }}"
# identifiers["plugin_namespace"] = "{{ cookiecutter.plugin_namespace }}"
identifiers["app_name"] = "{{ cookiecutter.app_name }}"

for key, value in identifiers.items():
    if not value.isidentifier():
        print(
            "ERROR: The %s ('%s') has to be a valid python identifier." % (key, value)
        )
        sys.exit(1)

if not re.match("[a-zA-Z.]+", "{{ cookiecutter.plugin_namespace }}"):
    print("ERROR: The plugin_namespace has to be a dotted python path.")
    sys.exit(1)
