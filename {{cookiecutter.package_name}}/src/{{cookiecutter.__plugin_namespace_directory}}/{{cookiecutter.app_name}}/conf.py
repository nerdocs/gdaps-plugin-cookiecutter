from django.test.signals import setting_changed
from gdaps.conf import PluginSettings

# This is a default conf file for a GDAPS plugin.
# You can use settings anywhere in your plugin using this syntax:
#
#     from .conf import {{ cookiecutter.app_name }}_settings
#
#     foo = {{ cookiecutter.app_name }}_settings.FOO_SETTING
#
# This way you can use custom (plugin-default) settings, that can be overridden globally if needed.


# required parameter.
NAMESPACE = '{{ cookiecutter.app_name.upper() }}'

# Optional defaults. Leave empty if not needed.
DEFAULTS = {
    # 'MY_SETTING': 'somevalue',
    # FIXME: plugin_namespace could be wrong here.
    # 'FOO_PATH': '{{ cookiecutter.plugin_namespace }}.{{ cookiecutter.app_name }}.models.FooModel',
    # 'BAR': [
    #     'baz',
    #     'buh',
    # ],
}

# Optional list of settings keys that are allowed to be in 'string import' notation. Leave empty if not needed.
IMPORT_STRINGS = (
    # 'FOO_PATH',
)

# Optional list of settings that have been removed. Leave empty if not needed.
REMOVED_SETTINGS = ()


{{ cookiecutter.app_name }}_settings = PluginSettings(namespace=NAMESPACE, defaults=DEFAULTS, import_strings=IMPORT_STRINGS)


def reload_{{ cookiecutter.app_name }}_settings(*args, **kwargs):
    setting = kwargs['setting']
    if setting == '{{ cookiecutter.app_name.upper() }}':
        {{ cookiecutter.app_name }}_settings.reload()


setting_changed.connect(reload_{{ cookiecutter.app_name }}_settings)
